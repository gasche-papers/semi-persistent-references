## First revision

In the first revision of the paper, I made the following changes:

1. a Related Work section, which mentions a paper from the 80s-90s
  mentioned by Review 3, and CVC and Facile and Colibri2. Thanks
  review 3 :-) If reviewers have other suggestions for related work to
  cover, I'm still happy to take suggestions.

  Facile and Colibri2 demonstrate that the basic idea of "backtracking
  references" was already present in the OCaml world, just no one
  thought of documenting their work clearly and packaging it
  separately from the rest of their codebase. I may have wasted my
  time redoing this, but the buck stops here.

2. a Benchmarks sections It took a lot of work, and the results are
  about as bland as I expected, but hopefully the reviewers are
  pleased to see that the work was done -- I guess it's more about
  putting the effort than about enjoying the result. Map is not just
  a "logarithmic overhead" slower, it is 13x slower in one arbitrary
  benchmark I wrote. There you go.

Now we are over the page limit and I will need to decide again which
content to cut. I am planning to cut the benchmark section, obviously,
and point readers to

  https://gitlab.inria.fr/gscherer/unionfind/-/blob/jfla-benchmarks/bench/README.Store.JFLA.md

instead. I will try to integrate the most, ahem, striking results from
the benchmark in the rest of the document (some found their way in the
Related Work section), to keep reassuring readers that benchmarks were
indeed written for this program. If reviewers have additional
suggestions for what insights from the benchmarks they would like to
see stated explicitly in the rest of the paper, they are warmly
welcome.


## Second revision

In the second revision I performed some relatively invasive changes to
the paper content, following the reviewers feedback -- in particular
grumpy reviewer 1. The paper looks better that way, thanks!

1. I do not call this work a "semi-persistent reference store" anymore
   but a "backtracking reference store". I now only use
   "semi-persistent" specifically to refer to the more-declarative-API
   style of Conchon and Filliatre. Their work is still fairly
   influencial, in particular they give all the vocabulary to think
   about how to specify this sort of
   data-structures. (Specifications are largely about the "ghost
   state", so they look similar for 'semi-persistent' APIs and for
   'backtracking' APIs.)

   This renaming also helps user which have heard of backtracking but
   not of semi-persistence.

2. The last optimization used to be called a "compact" optimization,
   which was a bad name. Now it is called a "record-elision
   optimization", which is much clearer. (Here "record" is in the
   sense of "recording something in a journal/log", not in the sense
   of "structure".) I reuse this vocabulary to describe
   similar-in-spirit optimizations that exist in related works.

I also made many small style changes, in particular the quotation
marks all over the place are now gone. (Not sure why I used them in
the first place, the mood of the moment?) Thanks for pointing them
out.

The discussion of the "early challenger" has also been streamlined,
I don't talk about arrays of size 1 anymore, this was a distraction.


## TODO list (third revision? only if there is time)

Some time-consuming remarks that I have not taken into account yet --
it is unclear if there will be time to do so.

1. notation change for the state-assertion comments
   => done
2. trying an implementation with HAMTs
   (to implement just backtracking references it's probably going to
   be slow, the cost of hashing may dominate get/set, but
   a fun exercise anyway)
3. a nice picture to represent the "stack and indices" structure
   => done

> ----------------------- REVIEW 1 ---------------------
> SUBMISSION: 9677
> TITLE: Semi-persistent references
> AUTHORS: Gabriel Scherer
> 
> ----------- Overall evaluation -----------
> SCORE: -1 (weak reject)
> ----- TEXT:
> Quelques remarques et commentaires.
> 
>   R1. Évaluation de performance
> 
>   La question des performances revient de façon récurrente dans
>   l'article mais celles-ci ne sont jamais formellement évaluées. À mon
>   sens, c'est *absolument nécessaire*.

DONE (reste à trouver comment rentrer en 20 pages)

>   R1'. "Interface(s)" (Section 4)
> 
>   La Section 4 ne me semble pas essentielle pour le propos de
>   l'article et elle est par ailleurs assez laborieuse (pour qui n'est
>   pas expert des APIs à l'oeuvre). La Section 4.2 est particulièrement
>   indigeste.

Tant pis, j'aime bien cette section donc je la garde.

>   S'il faut faire de la place pour une section d'évaluation de
>   performance, on pourra avantageusement la réduire ou la supprimer.
> 
>   R2. "there is a generic (instant?) way"
> 
>   Je ne comprends pas le sens de "instant" ici.

J'ai viré "instant'

>   R3. Typographie
> 
>   Il serait appréciable d'utiliser une police de caractère distincte
>   pour les noms de librairies, par exemple \textsf{unionFind} ou
>   \texttt{unionFind}.
> 
>   De même pour les noms de module OCaml (StoreRef,
>   StoreTransactionRef) : une typographie différenciée serait
>   appréciable.

J'ai essayé de faire ça.

>   R4. "semi-persistent"
> 
>   En Section 1.1, il serait utile de rappeler la définition de
>   semi-persistence. On pourra certainement intégrer la remarque
>   "Remark : persistent vs. semi-persisteent" à ce stade.

Finalement je parle moins de "semi-persistence" et plus de
backtracking", ce qui évite le problème du manque de familiarité.

>   R5. Introduction
> 
>   L'introduction est tortueuse : il y a une longue motivation du
>   problème (implémenter union-find, Sections 1.1 et 1.2) et une longue
>   description de deux non-solutions (Section 1.4). Le problème en
>   lui-même n'est présenté qu'en Section 2. Je serais en faveur de
>   montrer le problème, avec du code, bien plus tôt, dès
>   l'introduction. Le lien avec l'implémentation d'union-find étant
>   finalement très ténu, il pourrait même être relégué à une section
>   très ultérieure, du type "Evaluation".

J'ai rajouté des exemples de code -- dans le contexte d'union-find.

J'aime bien l'idée de partir d'un besoin réel, et je n'ai pas le temps
de récrire l'article dans le style proposé de toute façon, c'est un
changement de trop grande envergure.

>   R6. Section 2.2
> 
>   L'interface présentée ici est considérée comme figée ? Par exemple,
>   la sémantique subtile de `commit` et `rollback` imposant un appel
>   explicite à `terminate_nodiff` n'est pas négociable car imposée par
>   la librairie 'unionFind' ?

J'hésite à virer terminate_nodiff de la spec, pour montrer seulement
terminate_commit et terminate_rollback. Leurs préconditions sont plus
jolies, mais les specs sont quand même moins claires à mon goût;
`commit` et `rollback` ont une spec particulièrement claire, et le
fait qu'on puisse les appeler plusieurs fois sans changer de version
augmente un peu l'expressivité.

> 
>   R7. "a ``fast path'' that could be part of a refined specification" (p.8)
> 
>   Il semble raisonnable d'intégrer cette discussion à la spécification
>   donnée en 2.1-2.2. Il ne me semble pas judicieux d'éparpiller la
>   spécification.

C'est une bonne idée, fait.
  
>   R8. "Branch and terminate" (p.8)
> 
>   Pas de commentaires ? C'est très aride.

Parfois le code parle de lui-même quand il est simple, ça ne me semble
pas choquant de n'expliquer que ce qui bénéficie à être expliqué.

>   R9. "terminate_nodiff"
> 
>   Peut-on écrire une assertion qui valide la supposition que l'état de
>   la version courante est égale à l'état de son parent ? (comme
>   spécifié en Section 2.2). Cela permettrait de traduire concrètement
>   les invariants et hypothèses opérationnelles données dans la
>   spécification.

J'ai rajouté la remarque que j'avais enlevée à ce sujet pour gagner de la place.

> 
>   Par exemple, p.8, on aurait `assert Stack.is_empty _diff` avant
>   `s := rest` avec la paire de commentaires OCaml "current state :
>   (...)" et "final state : (...)" sur le modèle de rollback et commit.
> 
>   Pour la seconde et troisième version, on aurait `Stack.length
>   s.journal = List.hd s.children`.
> 
>   R10. `rollback s` (p.8)
> 
>   Le code donné est
> 
>   ```
>   while not (Stack.is_empty current_diff) do
>     match Stack.top current_diff with
>     | ...
>   done
>   ```
> 
>   Il s'agit plutôt de `Stack.pop`, n'est-ce pas ? Je recommande
>   (vivement !) d'utiliser l'inclusion de fichiers (testés !) avec
>   lstlistings
> 
>   https://en.wikibooks.org/wiki/LaTeX/Source_Code_Listings#Automating_file_inclusion
> 
>   pour éviter ce genre de situation embarrassante.

En théorie l'inclusion de fichiers testés c'est super, en pratique les
bouts de code viennent de commit différents d'un git, et les
commentaires ont changé de forme entre la version git (qui contient de
l'ASCI-art) et la version article (qui contient du tikz-art)... Ce
serait super délicat de trouver comment faire ça, donc je laisse en
l'état.

> 
>   R11. "state SC", "state SP", "state SR", "state SPP"
> 
>   Que signifient "SC", "SP", "SR", "SPP" dans les commentaires OCaml ?
>   Ce sont juste des noms de variables existentiellement quantifiées ?
>   L'utilisation de lettres majuscules est surprenant, pourquoi pas
>   $s_\mathrm{current}$, $s_\mathrm{parent}$, $s_\mathrm{root}$,
>   $s_\mathrm{gparent}$, etc. ?

Il faut voir si ça tient en place horizontale...
TODO

> 
>   R12. Section 3.2
> 
>   Une représentation illustrée d'un habitant de type `store` serait
>   très bienvenue : il y a trop d'invariants structurels encodés dans
>   des entiers pour que cela puisse être capturé par le pseudo-schéma
>   textuel en p.10.

TODO

>   R13. Bibliographie
> 
>   La bibliographie est déraisonnablement légère.
> 
>   Typos :
>   - Ponctuation française : un \usepackage[french]{babel} oublié ?

indeed!

>   - "1.1 Our quest: (..)" : sous-section sans chapô, peu élégant. On
>     peut supprimer le \subsection{..}, sans perte d'information.

ok

>   - Capitalisation de "Union-Find", "union-Find" : pourquoi le
>     Camel-Case (aléatoire) ?

mettons toujours Union-Find

>   - "versions.In fact" (p.2)

done

>   - "semi-persisteent" (p.4)

done
 
>   - il est fait un usage trop "fréquent" des "guillemets".

done

>     De même, l'usage de "Note :" alourdit la lecture.

je n'ai pas d'idée simple pour faire mieux

>   - "`move_stack` is traverses" (p.9)

done
> 
>   - "the list `s.childre`" (p.10)

done

>   - "neglictible" (p.13)

un mot qui existe presque... done

>   - "One expresses the other the transactional API" (p.14)

done

> 
> 
> ----------------------- REVIEW 2 ---------------------
> SUBMISSION: 9677
> TITLE: Semi-persistent references
> AUTHORS: Gabriel Scherer
> 
> ----------- Overall evaluation -----------
> SCORE: 2 (accept)
> ----- TEXT:
> 
> This article presents algorithms and implementations for
>   semi-persistent references, which supports both imperative updates,
>   and a limited notion of store versioning, along with commits and
>   rollback operations on the store.
> 
>   I found the presentation easy to read: it nicely present the issues
>   that semi-persistent structures are trying to solve and how it is
>   accomplished.  The algorithmic techniques presented are nice, and
>   would definitely deserve further investigation.
> 
>   I found three things missing in the article:
>   - I would have appreciated a bigger code example to ease the reader into it.

Il y a maintenant des exemples dans l'introduction. Pas gros, mais au moins des exemples...

>   - I doesn't present any benchmark. It *mentions* some performance aspects, but never formally
>   present them. In particular, I would have liked some actual number comparing the different
>   versions, and also the direct approach using a fully persistent store.

done.

>   - It doesn't mention any other algorithmic work appart from Cochon and Filliatre.
>   There are many modern data-structure with "partial" persistency,
>   such as HAMT for instance (one can find an implementain in containers), which could also conveniently
>   serve are backend for the unionFind library.

TODO benchmark backtracking references based on HAMTs

>   The interest of your semi-persistent references are that they could
>   be used as building blocks for these more complexe data-structures.
>   I would have liked both a more complete state of the art, and to go
>   further than just Semi persitent arrays, which are just the start of
>   the story there.
> 
>   Misc remarks:
>   - In the first implementation of `terminate_nodiff`, page 8, I was
>   surprised to have no failure case if the Stack `_diff` is non empty,
>   as described by the specification.

done added a remark about this in the paper

>   - Wouldn't be beneficial to implement diffs as (physical) maps over
>   references, instead of adding a field? This would naturally record
>   only the first `set`. Making it work with the last optimisation
>   might be chalenging but seems feasible.

I'm not sure what the suggestion is. By default (until the "last
optimization") we don't add a field to references, which are
represented exactly like in OCaml, we store backtracking information
in a log stack in the "reference store".

We could try to store the backtracking information inside each
reference, as an extra field or with this suggested map (but we really
don't want to pay a map lookup on each 'set'). But then we would still
need to record somewhere the list of references updated in the last
version, so that we can revert them properly on rollback.

Another approach would be to not revert them during rollback but on
the next 'get', start by handling logs more recent than the current
version. But this would be again rather slower I think, as it makes
'get' slower in the common case.

> ----------------------- REVIEW 3 ---------------------
> SUBMISSION: 9677
> TITLE: Semi-persistent references
> AUTHORS: Gabriel Scherer
> 
> ----------- Overall evaluation -----------
> SCORE: 1 (weak accept)
> ----- TEXT:
> The paper present generic handling of backtrackable reference in OCaml. It
> describes different version of the implementation. The paper aims at solving a
> real problem, type checking constructions that requires backtracking in a
> real setting (OCaml typechecker).
> 
> The paper is well written, except that it finishes abruptly. Also it is strange
> that no comparison between the solutions are provided.
> 
> It is unfortunate that the paper doesn't discuss solutions found in provers where
> backtracking is pervasive:
> -  CVC5 uses a similar solution to the paper (named context):
>    section 2.4 https://www-cs.stanford.edu/~preiner/publications/2022/BarbosaBBKLMMMN-TACAS22.pdf
> - Gambosi, G., Italiano, G. F., & Talamo, M. (1988, February). Getting back to
>   the past in the union-find problem. In Annual Symposium on Theoretical Aspects
>   of Computer Science (pp. 8-17). Springer, Berlin, Heidelberg.
> - Gambosi, G., Italiano, G. F., & Talamo, M. (1989). Worst-case analysis of the
>   set-union problem with extended backtracking. Theoretical computer science,
>   68(1), 57-70.
> - I haven't looked at how it is implemented but FaCiLe features backtrackable
>  references:
>      BARNIER, Nicolas et BRISSET, Pascal. FaCiLe: A Functional Constraint Library Release 1.1. École Nationale de l’Aviation Civile, 2004, p. 4-49.
> 
> The following work does not need a citation since they are not published, but it
> could be still interesting to the author of the paper (also consider the author of
> this work admonished to publish it):
> - Context in OCaml https://git.frama-c.com/pub/colibrics/-/blob/bobot/abs/colibri2/stdlib/context.mli
> - A proved version of the array with journal versioning: https://git.frama-c.com/pub/colibrics/-/blob/bobot/abs/colibrics/lib/utils/tagtbl.mlw#L230
> 
> 
> So even if related work is lacking, the goal is interesting and the different
> solutions informative. However a comparison between the solutions would greatly
> improved the paper.


