FILES=paper.tex

LATEXMK=latexmk -pdf -pdflatex="pdflatex --shell-escape %O %S" -bibtex
.PHONY: mk
mk:
	$(LATEXMK) $(FILES)

.PHONY: paper-diff.pdf
paper-diff.pdf: paper-diff.tex
	$(LATEXMK) paper-diff.tex

LATEXDIFF=latexdiff --allow-spaces --type=CCHANGEBAR --config "PICTUREENV=(?:(?:picture[\w\d*@]*)|(?:tikzpicture[\w\d*@]*)|(?:DIFnomarkup)|(?:mathpar)|(?:mathline)|(?:verbatim))"


paper-diff.tex: paper.tex paper-old.tex
	cp paper.tex paper-new.tex
	sed "s,{lstlisting},{verbatim},g" -i paper-new.tex
	$(LATEXDIFF) paper-old.tex paper-new.tex > paper-diff.tex
	rm paper-new.tex paper-old.tex

.PHONY: paper-old.tex
paper-old.tex:
	git show jfla-submission:paper.tex > paper-old.tex


clean:
	latexmk -c
	rm -f *.{log,bbl,nav,rev,snm,vrb,vtc}

.PHONY: all clean
